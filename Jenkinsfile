pipeline{
    options
            {
                buildDiscarder(logRotator(numToKeepStr: '6'))
                disableConcurrentBuilds()
            }
    agent any
    parameters {

        string(name: 'SLACK_CHANNEL_1',
                description: 'Canal no Slack para notificações do Jenkins',
                defaultValue: '#devops')
    }
    environment {
        VERSION = readMavenPom().getParent().getVersion()
        IMAGE = readMavenPom().getArtifactId()
        DEPLOYMENT_NAME = readMavenPom().getArtifactId()

        NEXUS_URL = "http://maven.southsystem.com.br:8081/repository/releases/"

        // Slack configuration
        SLACK_COLOR_DANGER  = '#E01563'
        SLACK_COLOR_INFO    = '#6ECADC'
        SLACK_COLOR_WARNING = '#FFC300'
        SLACK_COLOR_GOOD    = '#3EB991'
    }
    stages{
        stage("Iniciando"){
            steps{
                script {
                    committerEmail = sh (
                            script: 'git --no-pager show -s --format=\'%ae\'',
                            returnStdout: true
                    ).trim()
                    env.USER_ID = "${committerEmail}"
                }
                slackSend (color: "${env.SLACK_COLOR_INFO}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*STARTED:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")

            }
        }
        stage("Build/Tests"){
            when{
                branch "master"
            }
            steps{
                sh 'mvn clean install -s settings.xml -U'
            }
        }

        stage("Sonarqube"){
            when{
                branch "master"
            }
            steps{
                sh 'mvn clean verify sonar:sonar -s settings.xml -DskipTests'
            }
        }
        stage("Deploy Nexus"){
            when{
                branch "master"
            }
            steps{
                sh """
                    mvn clean install -s settings.xml -U
                    mvn deploy:deploy-file -DgeneratePom=false -DrepositoryId=nexus -Durl=${NEXUS_URL} -s settings.xml -DpomFile=pom.xml -Dfile=target/${DEPLOYMENT_NAME}-${VERSION}.jar
                """
            }
        }
    }
    post {
        always {
            deleteDir()
        }
        success{
            script {
                emailext (
                        subject: "[SUCCESS]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "devops@southsystem.com.br",
                        attachLog: true
                )
                emailext (
                        subject: "[SUCCESS]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "${committerEmail}",
                        attachLog: true
                )
                slackSend (color: "${env.SLACK_COLOR_GOOD}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*SUCCESS:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
            }
        }
        failure{
            script {
                emailext (
                        subject: "[FAILURE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "devops@southsystem.com.br",
                        attachLog: true
                )
                emailext (
                        subject: "[FAILURE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "${committerEmail}",
                        attachLog: true
                )
                slackSend (color: "${env.SLACK_COLOR_DANGER}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*FAILED:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
            }
        }
        unstable{
            script {
                emailext (
                        subject: "[UNSTABLE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "devops@southsystem.com.br",
                        attachLog: true
                )
                emailext (
                        subject: "[UNSTABLE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "${committerEmail}",
                        attachLog: true
                )
                slackSend (color: "${env.SLACK_COLOR_WARNING}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*UNSTABLE:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
            }
        }
    }
}