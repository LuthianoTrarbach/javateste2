package ${package}.config.broker;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface BrokerOutput {

	String OUTPUT_BROKER = "exchangeOutPut";
	
	@Output(BrokerOutput.OUTPUT_BROKER)
	MessageChannel outPutBroker();

}
